from django.db import models


class Ads(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=100)
    price = models.FloatField(default=0.0)
    description = models.CharField(max_length=1000)
    address = models.CharField(max_length=200)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Categories(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
