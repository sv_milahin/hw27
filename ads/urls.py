from django.urls import path
from ads import views
from ads import views

urlpatterns = [
    path('', views.home),
    path('cat/', views.CategoriesView.as_view()),
    path('cat/<int:pk>/', views.CategoriesDetailView.as_view()),
    path('ad/', views.AdsView.as_view()),
    path('ad/<int:pk>/', views.AdsDetailView.as_view()),
]
