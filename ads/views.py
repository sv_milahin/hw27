import json

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from ads import models


def home(request):
    return JsonResponse({'status': 'ok'}, status=200)


@method_decorator(csrf_exempt, name='dispatch')
class AdsView(generic.View):
    model = models.Ads

    def get(self, request):
        response = [
            {
                'id': item.id,
                'name': item.name,
                'author': item.author,
                'price': item.price,
                'description': item.description,
                'address': item.address,
                'is_published': item.is_published,
            } for item in models.Ads.objects.all()
        ]
        return JsonResponse(response, safe=False)

    def post(self, request):
        data = json.loads(request.body)
        ads = models.Ads()
        ads.name = data.get('name')
        ads.author = data.get('author')
        ads.price = data.get('price')
        ads.description = data.get('description')
        ads.address = data.get('address')
        ads.is_published = data.get('is_published')
        ads.save()
        response = {
            'id': ads.id,
            'name': ads.name,
            'author': ads.author,
            'price': ads.price,
            'description': ads.description,
            'address': ads.address,
            'is_published': ads.is_published,
        }
        return JsonResponse(response, safe=False)


class AdsDetailView(generic.DetailView):
    model = models.Ads

    def get(self, request, pk, *args, **kwargs):
        ads = get_object_or_404(models.Ads, pk=pk)
        response = {
                'id': ads.id,
                'name': ads.name,
                'author': ads.author,
                'price': ads.price,
                'description': ads.description,
                'address': ads.address,
                'is_published': ads.is_published,
            }
        return JsonResponse(response, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class CategoriesView(generic.View):
    model = models.Categories

    def get(self, request):
        response = [
            {
                'id': item.id,
                'name': item.name,
            } for item in models.Categories.objects.all()
        ]
        return JsonResponse(response, safe=False)

    def post(self, request):
        data = json.loads(request.body)
        cat = models.Categories()
        cat.name = data.get('name')
        cat.save()
        response = {
            'id': cat.id,
            'name': cat.name,
               }
        return JsonResponse(response, safe=False)


class CategoriesDetailView(generic.DetailView):
    model = models.Categories

    def get(self, request, pk, *args, **kwargs):
        categories = get_object_or_404(models.Categories, pk=pk)
        response = {
                'id': categories.id,
                'name': categories.name
             }

        return JsonResponse(response, safe=False)
