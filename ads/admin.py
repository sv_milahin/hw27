from django.contrib import admin

from ads.models import Ads, Categories

# Register your models here.
admin.site.register(Ads)
admin.site.register(Categories)